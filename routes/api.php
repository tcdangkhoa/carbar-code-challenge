<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'stripe-customer' , 'as' => 'stripe-customer'] , function() {
    Route::get('getList' , ['as' => 'getList' , 'uses' => 'App\Http\Controllers\StripeCustomerController@index']);
    Route::get('detail/{id}' , ['as' => 'detail' , 'uses' => 'App\Http\Controllers\StripeCustomerController@detail']);
    Route::post('store' , ['as' => 'store' , 'uses' => 'App\Http\Controllers\StripeCustomerController@store']);
    Route::put('update/{id}' , ['as' => 'update' , 'uses' => 'App\Http\Controllers\StripeCustomerController@update']);
});