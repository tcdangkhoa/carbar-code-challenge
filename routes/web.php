<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/*Route::get('/' , ['as' => '/' , 'uses' => 'App\Http\Controllers\StripeCustomerController@index']);
Route::group(['prefix' => 'stripe-customer' , 'as' => 'stripe-customer.'] , function() {
    Route::get('create' , ['as' => 'create' , 'uses' => 'App\Http\Controllers\StripeCustomerController@create']);
    Route::post('store' , ['as' => 'store' , 'uses' => 'App\Http\Controllers\StripeCustomerController@store']);
    Route::get('edit/{id}' , ['as' => 'edit' , 'uses' => 'App\Http\Controllers\StripeCustomerController@edit']);
    //Route::post('update' , ['as' => 'update' , 'uses' => 'App\Http\Controllers\StripeCustomerController@update']);
    Route::get('detail/{id}' , ['as' => 'detail' , 'uses' => 'App\Http\Controllers\StripeCustomerController@detail']);
});*/