# CarBar code challenge

### Requirements
 - PHP 7(7.3 or 7.4)
 - Mysql 5
 - Apache

## Setup local environment
```
    # Clone source from bitbucket
    git clone https://tcdangkhoa@bitbucket.org/tcdangkhoa/carbar-code-challenge.git

    # Install dependency and lib(php strike sdk , faker)
    composer install (make sure composer is installed on your computer)

    # Create DB
    Access to mysql and create a DB for project , after that update DB config for .env file : 
        - DB_CONNECTION=mysql
        - DB_HOST=127.0.0.1
        - DB_PORT=3306
        - DB_DATABASE=
        - DB_USERNAME=
        - DB_PASSWORD=

    # Create your .env file

    # Create stripe account
    Go to : https://dashboard.stripe.com/register and register a stripe account -> get your publish_key and secret_key -> replace your publish_key and secret_key into ENV STRIPE_PUBLISH_KEY variable and STRIPE_SECRET_KEY variable

    # Run migration
    php artisan migrate

    # Start project
    php artisan serve

    # Access
        API endpoint to get all customer : http://localhost:8000/api/stripe-customer/getList
        API method : GET

        API endpoint to get detail customer : http://localhost:8000/api/stripe-customer/detail/{id}
        API method : GET

        API endpoint to create a customer : http://localhost:8000/api/stripe-customer/store
        API method : POST
        API params : email , name , description , balance

        API endpoint to update a customer : http://localhost:8000/api/stripe-customer/update/{id}
        API method : PUT
        API params : name , description , balance (can't update email)

    # Run unit test
    vendor/bin/phpunit ./tests/Unit/Http/Controllers/StripeCustomerControllerTest.php

```

## Noted
    - About API list all customer and API get detail a customer ... i think we can get all customer or get detail a customer from DB , no need get from stripe API . Because when we crete/update a customer we also create/update on stripe system by stripe API . So , if we get detail all customer from DB , it make this function is stable and faster