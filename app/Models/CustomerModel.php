<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'stripe_customer';
    protected $fillable = [
        'id',
        'stripe_customer_id',
        'email',
        'description',
        'name',
        'balance',
        'created_at',
        'updated_at'
    ];

    public $timestamps = true;
}
