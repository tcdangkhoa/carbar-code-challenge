<?php

namespace App\Http\Controllers;

use Config;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
//use App\Http\Requests\CustomereRequest;
use App\Repositories\Customer\CustomerRepositoryInterface;

class StripeCustomerController extends Controller
{
    protected $customerRepository;

    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function index()
    {
        Log::info('Entry : StripeCustomerController/index');
        try {
            //Get all stripe customer from DB
            $customers = $this->customerRepository->getAll();

            Log::info('Exit : StripeCustomerController/index');

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => 'Get all customers successfully',
                'data'    => $customers
            ]);
        } catch (\Exception $e) {
            Log::error('Error when get stripe customer list' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json(['error' => 'Error when get stripe customer list'] , Response::HTTP_BAD_REQUEST);
        }
    }

    private function getStripeObject()
    {
        //Create stripe object
        $stripe = new \Stripe\StripeClient(Config::get('services.stripe.secretKey'));
        return $stripe;
    }

    public function store(Request $request)
    {
        Log::info('Entry : StripeCustomerController/store');

        $validator = Validator::make($request->all() , [
            'email' => [
                'required',
                'regex:/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
                'max:50',
                'unique:stripe_customer'
            ]
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => $validator->errors()
            ]);
        }

        try {
            $mess = 'Create customer success';
            $data = $request->all();

            //Unset unused data
            unset($data['_token']);
            unset($data['submit']);

            $stripe = $this->getStripeObject();

            //Call to stripe API to create a customer
            $stripeCustomer = $stripe->customers->create([$data]);

            //Add customer id to $data to store to DB
            $data['stripe_customer_id'] = $stripeCustomer->id;

            //Store to DB
            $customer = $this->customerRepository->create($data);

            Log::info('Exit : StripeCustomerController/store');

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => 'Create a customer successfully',
                'data' => $customer
            ]);
        } catch(\Stripe\Exception\CardException $e) {
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
            ]);
        } catch (\Stripe\Exception\RateLimitException $e) {
            Log::error('Too many requests made to the API too quickly' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Too many requests made to the API too quickly'
            ]);
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            Log::error('Invalid parameters were supplied to Stripe API' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);return abort(500);
        } catch (\Stripe\Exception\AuthenticationException $e) {
            Log::error('Authentication with Stripe API failed' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            Log::error('Network communication with Stripe failed' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            Log::error('Error when call to stirpe API' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        } catch (\Exception $e) {
            Log::error('Error when create customer' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        }
    }

    public function update($id , Request $request)
    {
        Log::info('Entry : StripeCustomerController/store');

        try {
            $data = $request->all();
            $mess = 'Edit customer success';
            $customer = $this->customerRepository->find($id);

            $stripe = $this->getStripeObject();

            //Call to stripe API to update a customer
            $stripeCustomer = $stripe->customers->update($customer['stripe_customer_id'] , [$data]);

            //Force user can't update email
            if(isset($data['email'])) {
                unset($data['email']);
            }

            //Update in DB
            $customer = $this->customerRepository->update($id , $data);

            Log::info('Exit : StripeCustomerController/store');

            return response()->json([
                'code' => Response::HTTP_OK,
                'message' => 'Create a customer successfully',
                'data' => $customer
            ]);
        } catch(\Stripe\Exception\CardException $e) {
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
            ]);
        } catch (\Stripe\Exception\RateLimitException $e) {
            Log::error('Too many requests made to the API too quickly' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Too many requests made to the API too quickly'
            ]);
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            Log::error('Invalid parameters were supplied to Stripe API' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);return abort(500);
        } catch (\Stripe\Exception\AuthenticationException $e) {
            Log::error('Authentication with Stripe API failed' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            Log::error('Network communication with Stripe failed' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            Log::error('Error when call to stirpe API' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        } catch (\Exception $e) {
            Log::error('Error when update customer' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST
            ]);
        }
    }

    public function detail($id)
    {
        Log::info('Entry : StripeCustomerController/edit');
        try {
            //Get stripe customer data by id
            $customer = $this->customerRepository->find($id);

            if (!empty($customer)) {
                return response()->json([
                    'code' => Response::HTTP_OK,
                    'message' => 'Get detail customer successfully',
                    'data' => $customer
                ]);
            }

            Log::info('Exit : StripeCustomerController/edit');

            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Invalid customer ID',
                'data' => []
            ]);
        } catch (\Exception $e) {
            Log::error('Error when edit a customer' , ['message' => $e->getMessage() , 'file' => $e->getFile() , 'line' => $e->getLine()]);
            return response()->json([
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => 'Error when edit a customer',
            ]);
        }
    }
}
