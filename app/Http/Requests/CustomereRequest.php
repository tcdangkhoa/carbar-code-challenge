<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomereRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ($this->input('id')) ? '' : [
                'required',
                'regex:/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
                'max:50',
                'unique:stripe_customer'
            ]
        ];
    }

    public function messages()
    {
        return [
            /*'email.required' => 'Email is mandatory',
            'email.regex' => 'Email structure is invalid',
            'email.max' => 'Max length is 50 character'*/
            'email.unique' => 'The email is existed'
        ];
    }
}
