<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStripeCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_customer' , function (Blueprint $table) {
            $table->increments('id');
            $table->char('stripe_customer_id' , 100);
            $table->char('email' , 50);
            $table->string('description' , 255)->nullable();
            $table->char('name' , 50)->nullable();
            $table->char('balance' , 15)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stripe_customer');
    }
}
