<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Repositories\Customer\CustomerRepository;
use App\Models\CustomerModel;
use Illuminate\Http\Response;
use Faker\Factory as Faker;

class StripeCustomerControllerTest extends TestCase
{
    protected $customerRepository;
    protected $customer;

    public function setUp() : void
    {
        parent::setUp();
        $this->faker = Faker::create();

        $this->customerRepository = app(CustomerRepository::class);

        $this->customer = [
            'email' => $this->faker->unique()->safeEmail(),
            'name' => $this->faker->name,
            'description' => \Illuminate\Support\Str::random(11),
            'balance' => substr(rand() , 0 , 3)
        ];
    }

    public function tearDown() : void
    {
        parent::tearDown();
    }

    public function testCreate()
    {
        //Test case create customer without email
        $params = ['name' => 'test_name' , 'balance' => 111];
        $response = $this->post('/api/stripe-customer/store' , $params);
        $response->assertSee(Response::HTTP_BAD_REQUEST);
        $response->assertSee('The email field is required.');

        //Test case create customer with invalid email
        $params = ['email' => 'test_mail' , 'name' => 'test_name' , 'balance' => 111];
        $response = $this->post('/api/stripe-customer/store' , $params);
        $response->assertSee(Response::HTTP_BAD_REQUEST);
        $response->assertSee('The email format is invalid.');

        //Test case create with email - successfully
        $response = $this->post('/api/stripe-customer/store' , $this->customer);

        $response->assertSee(Response::HTTP_OK);
        $response->assertSee('Create a customer successfully');
        $response->assertSee($this->customer['email']);
        $response->assertSee($this->customer['name']);
        $response->assertSee($this->customer['description']);
        $response->assertSee($this->customer['balance']);

        /*$this->customer['stripe_customer_id'] = \Illuminate\Support\Str::random(11);
        $customer = $this->customerRepository->create($this->customer);
        $this->assertInstanceOf(CustomerModel::class , $customer);*/

        //Test case create customer with existed email
        $response = $this->post('/api/stripe-customer/store' , $this->customer);
        $response->assertSee(Response::HTTP_BAD_REQUEST);
        $response->assertSee('The email has already been taken.');
    }

    public function testGetDetailCustomer()
    {
        $this->customer['stripe_customer_id'] = \Illuminate\Support\Str::random(11);
        $customer = $this->customerRepository->create($this->customer);

        $createdCustomer = $this->customerRepository->find($customer->id);

        $this->assertInstanceOf(CustomerModel::class , $createdCustomer);

        $this->assertEquals($createdCustomer['email'] , $customer['email']);
        $this->assertEquals($createdCustomer['name'] , $customer['name']);
        $this->assertEquals($createdCustomer['balance'] , $customer['balance']);
        $this->assertEquals($createdCustomer['description'] , $customer['description']);
        $this->assertEquals($createdCustomer['stripe_customer_id'] , $customer['stripe_customer_id']);
    }


    public function testUpdate()
    {
        $this->customer['stripe_customer_id'] = \Illuminate\Support\Str::random(11);
        $customer = $this->customerRepository->create($this->customer);

        $updateData = ['email' => 'updateEmail@mail.com' , 'name' => 'update_name' , 'description' => 'update_description' , 'balance' => 'update_balance'];
        $updatedCustomer = $this->customerRepository->update($customer->id , $updateData);

        $this->assertInstanceOf(CustomerModel::class , $updatedCustomer);

        $this->assertEquals($updatedCustomer['name'] , 'update_name');
        $this->assertEquals($updatedCustomer['email'] , 'updateEmail@mail.com');
        $this->assertEquals($updatedCustomer['balance'] , 'update_balance');
        $this->assertEquals($updatedCustomer['description'] , 'update_description');
        $this->assertEquals($updatedCustomer['stripe_customer_id'] , $this->customer['stripe_customer_id']);
    }
}
