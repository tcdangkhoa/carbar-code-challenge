@if ($errors->any())
<p class="alert alert-error" style="margin: 58px 0 0">
    @foreach ($errors->all() as $err)
        {!! $err !!}<br />
    @endforeach
</p>
@endif

@if (isset($messageBagName) && $errors->hasBag($messageBagName) && count($errors->getBag($messageBagName)->getMessages()))
<p class="alert alert-error">
    @foreach ($errors->getBag($messageBagName)->getMessages() as $err)
        {!! $err[0] !!}<br />
    @endforeach
</p>
@endif