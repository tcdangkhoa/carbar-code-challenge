@if (!empty($messages))
    <div>
    @foreach ($messages as $msg)
        <p class="message">{!! $msg !!}</p>
    @endforeach
    </div>
@elseif(session('message'))
    <p class="message">{!! session('message') !!}</p>
@endif