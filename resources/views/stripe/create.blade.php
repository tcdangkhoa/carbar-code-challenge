<head>
    <meta charset="utf-8">
    <title>Create a customer</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
    
<main class="main" style="width: 100%; padding-left: 5%; padding-right: 5%">

    <h2 class="title" style="text-align: center;">
        Create a customer
    </h2>

    <div style="padding-left: 35%; padding-right: 45%">
        <form method="POST" action="{{ route('stripe-customer.store') }}">
            @csrf
            <table class="table">
                <tr>
                    <th>Email*</th>
                    <td>
                        <input type="text" name="email" value="{{ old('email') }}" class="input-text input-right">
                        @include('parts.formErrors', ['attribute' => 'email'])
                    </td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>
                        <input type="text" name="description" value="{{ old('description') }}" class="input-text input-right">
                    </td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>
                        <input type="text" name="name" value="{{ old('name') }}" class="input-text input-right">
                    </td>
                </tr>
                <tr>
                    <th>Balance</th>
                    <td>
                        <input type="number" name="balance" value="{{ old('balance') }}" class="input-text input-right">
                    </td>
                </tr>
            </table>
            <input type="submit" name="submit" value="submit" style="margin-left: 60%">
        </form>
    </div>
</main>