<head>
    <meta charset="utf-8">
    <title>Stripe customers list</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style type="text/css">
        .message {
            text-align: center;
            color: blue;
        }

        .alert {
            text-align: center;
            color: red;
        }
    </style>
</head>
    
<main class="main" style="width: 100%; padding-left: 5%; padding-right: 5%">

    <h2 class="title" style="text-align: center;">
        Lists All Customers
    </h2>

    <h2 class="title">
        <a href="{{ route('stripe-customer.create') }}" class="btn btn-primary btn-m" style="margin-left: 0px;">Create a customer</a>
    </h2>

    @include('parts.infoMsg')
    @include('parts.errorMsg')

        <table class="table">
            <tr>
                <th>Email</th>
                <th>Stripe customer ID</th>
                <th>Description</th>
                <th>Name</th>
                <th>Balance</th>
                <th>Updates a customer</th>
                <th>Retrieves a customer</th>
            </tr>

            @if(!$customers->isEmpty())
                @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->stripe_customer_id }}</td>
                        <td>{{ $customer->description }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->balance }}</td>
                        <td><a href="{{ route('stripe-customer.edit' , ['id' => $customer->id]) }}" class="btn btn-primary btn-m edit-category" style="font-size: 12px;">Updates a customer</a></td>
                        <td><a href="{{ route('stripe-customer.detail' , ['id' => $customer->id]) }}" class="btn btn-primary btn-m" style="font-size: 12px;">Retrieves a customer</a></td>
                    </tr>
                @endforeach
            @endif
        </table>
</main>