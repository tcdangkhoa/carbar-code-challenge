<head>
    <meta charset="utf-8">
    <title>Detail a customer</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
    
<main class="main" style="width: 100%; padding-left: 5%; padding-right: 5%">

    <h2 class="title" style="text-align: center;">
        Detail a customer
    </h2>

    <div style="padding-left: 35%; padding-right: 45%">
        <table class="table">
           <tr>
                <th>Email</th>
                <td>{{ $customer->email }}</td>
            </tr>
            <tr>
                <th>Stripe customer ID</th>
                <td>{{ $customer->stripe_customer_id }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $customer->description }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ $customer->name }}</td>
            </tr>
            <tr>
                <th>Balance</th>
                <td>{{ $customer->balance }}</td>
            </tr>
        </table>
        <a href="{{ route('/') }}" style="margin-left: 60%">OK</a>
    </div>
</main>